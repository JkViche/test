const fetchAPI = require("node-fetch");
const url = 'http://localhost:8080/events'
const validParameters = '2005-05-18/2008-03-25'
const invalidParameters =  'gluten/2007-02-15'

console.log('Valid request test: ')
test(`${url}/${validParameters}`).then(() => console.log('Invalid request test: '))
test(`${url}/${invalidParameters}`)

async function test(url : string) {

  const result = await fetchAPI(url).then( res => res.json())
                                    .then( res => console.log(res))
}
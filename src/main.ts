import * as restify from 'restify';
import * as CorsMiddleware from 'restify-cors-middleware';
import { routes } from './routes';

//const errors = require('restify-errors');
const port = 8080;
const server = restify.createServer({
    name: 'API Server'
    });
const corsMiddlewareOptions: CorsMiddleware.Options = {
    origins: ['*'],
    credentials: false,
    // tslint:disable-next-line:max-line-length
    allowHeaders: [
        'origin',
        'accept',
        'accept-version',
        'Accept-Encoding',
        'Accept-Language',
        'Access-Control-Allow-Origin',
        'ama-client-ref',
        'content-length',
        'content-md5',
        'content-type',
        'date',
        'x-api-version',
        'x-response-time',
        'x-pingother',
        'X-CSRF-Token',
        'authorization',
        'API-Token',
        'Authorization',
        'config',
        'scenario_mode',
        'scenario_name',
        'scenario_target',
    ],
    exposeHeaders: [
        'X-Api-Version',
        'X-Request-Id',
        'X-Response-Time',
        'x-api-key',
        'authorization',
        'Authorization',
        'Access-Control-Allow-Origin',
    ],
};
const cors: CorsMiddleware.CorsMiddleware = CorsMiddleware(corsMiddlewareOptions);

server.pre(cors.preflight);
server.use(cors.actual);

server.pre(async (req: restify.Request, res: restify.Response, next: restify.Next) => {
    req.accepts('text/plain');
    req.accepts('application/json');
    return next();
});

server.use(restify.plugins.bodyParser());

routes(server);

server.listen(port, function() {
    console.info('%s listening at %s', server.name, server.url);
});
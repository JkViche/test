//import * as restify from 'restify';
import { endpointHandler } from "../handlers/handlers"

//const fs = require('fs');

export const routes = (server: any) => {
    server.get(`/events/:startDate/:endDate`, endpointHandler);
}
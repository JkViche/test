import * as restify from 'restify';
import { isDate, isBefore,parse } from 'date-fns'

export const endpointHandler = async (req: restify.Request, res: restify.Response, next: restify.Next) => {
    let response : any
    let mockData = [
      [ { time: "20031216", count: 1}, { time: "20031216", count: 10 }, { time: "20031216", count: 2 } ], 
      [ { time: "20031216", count: 3}, { time: "20031216", count: 50 }, { time: "20031216", count: 21 } ],
      [ { time: "20031216", count: 8}, { time: "20031216", count: 0 }, { time: "20031216", count: 52 } ],
      [ { time: "20031216", count: 7}, { time: "20031216", count: 6 }, { time: "20031216", count: 5 } ],
      [ { time: "20031216", count: 12}, { time: "20031216", count: 17 }, { time: "20031216", count: 32 } ],
      [ { time: "20031216", count: 14}, { time: "20031216", count: 63 }, { time: "20031216", count: 23 } ],
      [ { time: "20031216", count: 31}, { time: "20031216", count: 24 }, { time: "20031216", count: 54 } ],
      [ { time: "20031216", count: 18}, { time: "20031216", count: 42 }, { time: "20031216", count: 19 } ],
      [ { time: "20031216", count: 87}, { time: "20031216", count: 60 }, { time: "20031216", count: 20 } ],
      [ { time: "20031216", count: 25}, { time: "20031216", count: 45 }, { time: "20031216", count: 28} ],
    ]
    try{
      const startDate = parse(req.params.startDate, "yyyy-MM-dd", new Date )
      const endDate = parse(req.params.endDate, "yyyy-MM-dd", new Date )
      if(isDate(startDate) && isDate(endDate) && startDate !== null && endDate !== null) {

        if(isBefore(startDate, endDate)) {
          let index = Math.floor( Math.random() * 9 )
          response = mockData[index]
        } else {
          response = { msg: 'The startDate should be before the endDate' }
        }

      } else {
        response = { msg: 'The request parameters should exclusively be dates' }
      }
    }catch (e) {
      response = { msg: 'Some information are missing in the request' }
    }finally {
      res.send(response);
      next();
    }
};

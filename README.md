# TEST

## Project setup
```
npm install
```

### Run server and hot-reloads for development
```
npm start
```

### Run the two test scenarios
```
npm test
```

TECHNICAL QUESTION

A. Lerna library is generally used with monorepos to handle different packages sometimes with a signle git repo in order to publish them at the same time.

B. Lerna differs from yarn workspaces in a sense that with yarn workspaces we don't need to bootstrap dependencies for each package. In fact, yarn workspaces manage all the dependencies in a single node_modules folder which can make the project handling easier particularly when it comes to install time.

C. As an alternantive to Lerna and workspaces we can use Bit component monorepo.

D. Lens libraries allow us to abstract state shape behind getters and setters in a software engineering project.

E. Drawbacks of ReactJS library:
    - The high pace of development: ReactJS is in a constant state of lux, with new features being introduced on a regular basis and old ones becoming deprecated.
    - Lack of convention: Because ReactJS is actively developed around the world there are few established conventions to speak
    - Steep learning curves: For a relatively "Young library" ReactJS is very large and its documentation is far from stellar.